This is a project for Microsoft Web Technologies created by a group of 4 students.

This is a website titled Conestoga Video Game Store in which users can create an account, create and join events, buy games, and add reviews.

To run the project, one must open the solution file included in the repo in Visual Studio and launch it on a IIS Server.

This software is provided under the MIT license and as such everything can be modified and used free of charge without any conditions. This choice was made as this software was developed as part of a College
Program and is was used to learn ASP and IIS.

Commit